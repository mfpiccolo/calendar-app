require 'spec_helper'

describe Note do

  context 'associations' do
    it {should belong_to :notable}
    it {should have_many :notes}

  end

  context 'validations' do
    it {should validate_presence_of :message}
  end

end