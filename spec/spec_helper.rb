require 'pg'
require 'rspec'
require 'active_record'
require 'shoulda-matchers'
require 'factory_girl'
require './spec/factories'
require './lib/event'
require './lib/to_do'
require './lib/note'


ActiveRecord::Base.establish_connection(YAML::load(File.open('./db/config.yml'))["test"])

RSpec.configure do |config|
  config.after(:each) do
    Event.all.each {|event| event.destroy}
    ToDo.all.each {|to_do| to_do.destroy}
    Note.all.each {|note| note.destroy}
  end
end