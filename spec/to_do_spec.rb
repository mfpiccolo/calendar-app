require 'spec_helper'

describe ToDo do

  context 'validations' do
    it {should validate_presence_of :name}
    it {should validate_presence_of :due_date_time}
  end

    context 'scopes' do 
    it 'sets all the to-dos to be in order by start_date' do 
      to_dos = ['01/02/2013', '01/01/2013'].map {|date| FactoryGirl.create(:to_do, :due_date_time => date)}
      ToDo.all.should eq to_dos.sort_by &:due_date_time
    end
  end
end