require 'spec_helper'

describe Event do

  context 'validations' do
    it {should validate_presence_of :name}
    it {should validate_presence_of :start_date}
    it {should validate_presence_of :end_date}
    it {should validate_presence_of :start_time}
    it {should validate_presence_of :end_time}
    
    it 'checks if a start_date is before an end_date' do 
      event = FactoryGirl.build(:event, :start_date => "03/02/2013", :end_date => "01/02/2013")
      event.should_not be_valid
    end

    it 'checks if a start_date is before an end_date' do 
      event = FactoryGirl.build(:event, :start_date => "01/02/2013", :end_date => "03/02/2013")
      event.should be_valid
    end
  end  

  context 'scopes' do 
    it 'sets all the events to be in order by start_date' do 
      events = ['01/02/2013', '01/01/2013'].map {|date| FactoryGirl.create(:event, :start_date => date, :end_date => '01/07/2013')}
      Event.all.should eq events.sort_by &:start_date
    end
  end  
end

