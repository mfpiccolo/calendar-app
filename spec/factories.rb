FactoryGirl.define do
  factory :event do
    sequence(:name) {|n| "event#{n}"}
    sequence(:start_date) {|n| "0#{n}/01/2008"} 
    sequence(:end_date) {|n| "0#{n}/01/2008"} 
    sequence(:start_time) {|n| "#{n}:30"}
    sequence(:end_time) {|n| "#{n}:30"}


    factory :event_with_notes do
      after(:create) {|event| FactoryGirl.create(:note, :notable => event)}
    end
  end

  factory :note do
    sequence(:message) {|n| "#{n}"}
     # notable {FactoryGirl.create :event}
  end

  factory :to_do do
    sequence(:name) {|n| "name#{n}"}
    sequence(:due_date_time) {|n| "01/0#{n}/2013"}
  end
end

