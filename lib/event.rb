class Event < ActiveRecord::Base
  has_many :notes, :as => :notable

  validates :name, :start_date, :end_date, :start_time, :end_time, :presence => true
  validate :start_must_be_before_end_date

  default_scope(:order => :start_date)

  private

  def start_must_be_before_end_date
    unless start_date.nil? && end_date.nil?
      errors.add(:start_date, "must be before end time") if start_date > end_date
    end 
  end
end