class ToDo < ActiveRecord::Base
  has_many :notes, :as => :notable
  
  validates :name, :due_date_time, :presence => true

  default_scope(:order => :due_date_time)

  def read_notes
    unless self.notes.length == 0
      self.notes.map {|note| note.message}.join(', ')
    end
  end
end