require 'pg'
require 'active_record'
require 'time'
require '../lib/event'
require '../lib/note'
require '../lib/to_do'
require './todo_ui'
require './event_ui'
require './note_ui'

database_configurations = YAML::load(File.open('../db/config.yml'))
development_configuation = database_configurations["development"]
ActiveRecord::Base.establish_connection(development_configuation)