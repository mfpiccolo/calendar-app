require './ui_helper'

def to_do_menu
  choice = nil
  until choice == 'x'
    puts "Press 'a' add a To-Do and 'l' to list and update existing To-Dos, 'd' to delete a To-Do or 's' to search by name."
    choice = gets.chomp
    case choice
    when 'a'
      add_update_to_do
    when 'l'
      list_update_to_dos
    when 's'
      search_to_dos
    when 'd'
      delete_to_do
    when 'x'
      exit
    else
      invalid
    end
  end
end

def add_update_to_do(id = nil)
  puts "Enter the name of the To-Do:"
  name = gets.chomp.capitalize
  puts "Enter the due date: (DD/MM/YYYY)"
  due_date = gets.chomp
  puts "Enter the due time: (e.g. 2:30 pm)"
  puts "(Note: default is 12:00 pm)"
  due_time = gets.chomp
  due_time = '12:00:00' if due_time.empty? 
  due_date_time = due_date + " " + due_time
  if id.nil?
    to_do = ToDo.new(:name => name, :due_date_time => due_date_time)
    save_to_do(to_do)
  else
    to_do = ToDo.update(id, :name => name, :due_date_time => due_date_time)
    create_note(to_do)
  end
end

def save_to_do(to_do)
  if to_do.save
    puts "#{to_do.name} has been added to your calendar"
    puts "on #{to_do.due_date_time.strftime("%d-%m-%Y")}"
    puts "on #{to_do.due_date_time.strftime("%H:%M")}.\n\n"
    create_note(to_do)
  else
    puts "Invalid To-Do. Here's what's wrong:"
    to_do.errors.full_messages.each {|message| puts message}
    puts "\nTry again. Do it right this time!\n\n"
    add_to_do
  end
end

def list_update_to_dos
  if ToDo.all.length >= 1 
    puts "Here are your To-Dos"
    display_to_dos(ToDo.order("due_date_time"))
    puts "Would you like to edit a to_do? (Y/N)"
    choice = gets.chomp.upcase
    if choice == 'Y'
      puts 'please enter the id number of the to_do you would like to edit:'
      id = gets.chomp
      add_update_to_do(id)
    end
  else
    puts "You have no To-Dos."
  end
end 

def delete_to_do
  if ToDo.all.length >= 1 
    list_to_dos
    puts "Please enter the ID number you would like to delete:"
    id = gets.chomp.to_i
    ToDo.find(id).destroy
  else
    puts "You have no To-Dos."
  end
end

def display_to_dos(to_dos)
  to_dos.each {|to_do| puts "ID: #{to_do.id}\nName: #{to_do.name}\nStarts: on #{to_do.due_date_time.strftime("%d-%m-%Y")} @ #{to_do.due_date_time.strftime("%H:%M")}\nNote: #{to_do.notes.map {|note| note.message}.join("\nNote: ")}\n\n"}
end

def search_to_dos
  puts "What is the name of the to-do you are looking for?"
  to_dos = ToDo.where(:name => gets.chomp)
  display_to_dos(to_dos)
end

