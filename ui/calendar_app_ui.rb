require './ui_helper'

def welcome
  puts "Welcome to the Epicodus Calendar App."
  menu
end

def menu
  choice = nil
  until choice == 'x'
    puts "Press 'e' for Events and 't' for To-Dos or 'x' to exit."
    choice = gets.chomp
    case choice
    when 'e'
      event_menu
    when 't'
      to_do_menu
    when 'x'
      exit
    else
      invalid
    end
  end
end

def invalid
  puts "That was an invalid choice"
  menu
end

welcome