require './ui_helper'


def create_note(notable)
  puts "Would you like to add a note? (Y/N): "
  choice = gets.chomp.upcase 
  if choice == 'Y'
    puts "Enter the message:"
    memo = gets.chomp
    new_note = notable.notes.create(:message => memo)
    if new_note.save
      puts "your message was attached."
      puts "Would you like to put a note on a note for some crazy reason? (Y/N)"
      choice = gets.chomp.upcase
      if choice == 'Y'
        create_note(new_note)
      end
    else
      new_note.errors.full_messages.each {|message| puts message}
    end
  end
end
