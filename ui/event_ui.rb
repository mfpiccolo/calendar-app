require './ui_helper'

def event_menu
  choice = nil
  until choice == 'x'
    puts "Press 'a' add an event and 'l' to list and edit existing events, 'd' to delete an event or 's' for searching by event name."
    choice = gets.chomp
    case choice
    when 'a'
      add_update_event
    when 's'
      search_event
    when 'l'
      list_events_update
    when 'd'
      delete_event
    when 'x'
      exit
    else
      puts "Invalid entry!. Try again. Do it right this time!\n\n"
    end
  end
end

def add_update_event(id = nil)
  @start_time = '0:00:01'
  @end_time = '23:59:59'
  puts "Enter event name:"
  @name = gets.chomp.capitalize
  puts "Enter the date: (DD/MM/YYYY)"
  @start_date = gets.chomp
  puts "Is this a multi-day event? (Y/N): "
  choice = gets.chomp.upcase
  if choice == 'Y'
    puts "Enter the end date:"
    @end_date = gets.chomp
  else
    @end_date = @start_date
    puts "Is this an all-day event? (Y/N):"
    choice = gets.chomp.upcase
    unless choice == 'Y'
      puts "Enter the start time: (e.g. 9:30 pm)"
      @start_time = gets.chomp
      puts "Enter the end time: "
      @end_time = gets.chomp
    end
  end
  if id.nil?
    event = Event.new(:name => @name, :start_date => @start_date, :end_date => @end_date, :start_time => @start_time, :end_time => @end_time)
    save_event(event)
  else
    event = Event.update(id, :name => @name, :start_date => @start_date, :end_date => @end_date, :start_time => @start_time, :end_time => @end_time)
    create_note(event)
  end
end

def save_event(event)
  if event.save
    puts "#{event.name} has been added to your calendar.\n\n"
    create_note(event)
  else
    puts "Invalid event. Here's what's wrong:"
    event.errors.full_messages.each {|message| puts message}
    puts "\nTry again. Do it right this time!\n\n"
    add_event
  end
end

def list_events_update
  if Event.all.length >= 1
    puts "Here are your Events"
    display_events(Event.order("start_date"))
    puts "Would you like to edit an event? (Y/N)"
    choice = gets.chomp.upcase
    if choice == 'Y'
      puts 'please enter the id number of the event you would like to edit:'
      id = gets.chomp
      add_update_event(id)
    end
  else
    puts "You have no Events."
  end
end 

def delete_event
  if Event.all.length >= 1
    list_events
    puts "Please enter the ID number you would like to delete:"
    id = gets.chomp.to_i
    Event.find(id).destroy
  else
    puts "You have no Events."
  end
end

def display_events(events)
  events.each do |event|
    puts "ID: #{event.id}\nName: #{event.name}\nStarts: on #{event.start_date} @ #{event.start_time.strftime("%H:%M")}\nEnds: on #{event.end_date} @ #{event.end_time.strftime("%H:%M")}\n"
    event.notes.each { |note| display_note(note)}
  end 
end

def display_note(notable, tab = "")
  puts "#{tab}Note: #{notable.message}"
  notable.notes.each do |note|
    tab += "\t"
    display_note(note, tab)
  end
end

def search_event
  puts "What is the name of the event you are looking for?"
  events = Event.where(:name => gets.chomp)
  display_events(events)
end

