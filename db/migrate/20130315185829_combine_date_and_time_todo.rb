require './lib/to_do'

class CombineDateAndTimeTodo < ActiveRecord::Migration
  def change  
    add_column :to_dos, :due_date_time, :datetime

    ToDo.all.each {|to_do| to_do.update_attributes(:due_date_time => to_do.due_date.to_s + " " + to_do.due_time.hour.to_s + ":" + to_do.due_time.min.to_s) }
  end
end
