class DropTablesDateAndTime < ActiveRecord::Migration
  def change
    remove_column :to_dos, :due_date, :due_time
  end
end