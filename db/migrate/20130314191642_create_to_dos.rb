class CreateToDos < ActiveRecord::Migration
  def change
    create_table :to_dos do |t|
      t.column :name, :string
      t.column :due_date, :date
      t.column :due_time, :time

      t.timestamps
    end
  end
end
